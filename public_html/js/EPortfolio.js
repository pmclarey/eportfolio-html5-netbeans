var PLAY_INTERVAL = 3000;
var slideShowList = [];
var mainContent = document.getElementById("content");
var navlist = document.getElementById("navlist");
var globalXHR = new XMLHttpRequest();
var pageXHR = new XMLHttpRequest();
globalXHR.open('GET', './data/global.json', true);
pageXHR.open('GET', getPageJsonFileName(), true);
globalXHR.send();
pageXHR.send();

function getPageJsonFileName() {
    var path = location.pathname;
    var page = path.substring(path.lastIndexOf('/') + 1);
    return "./data/" + page.substring(0, page.indexOf('.') + 1) + "json";
}

globalXHR.onload = function() {
    var globalData = JSON.parse(this.responseText);
    loadGlobalData(globalData);
};

pageXHR.onload = function() {
    var pageData = JSON.parse(this.responseText);
    loadPageData(pageData);
};

function loadGlobalData(globalData) {
    bannerImage = document.getElementById("bannerImage");
    banner = document.getElementById("banner");
    bannerImage.src = globalData.bannerImagePath;
    bannerImage.alt = globalData.name + "'s ePortfolio Banner";
    banner.innerHTML = globalData.name;
    globalData.links.forEach(addLink);
}

function loadPageData(pageData) {
    document.title = pageData.title;
    pageData.stylesheets.forEach(addStylesheet);
    pageData.components.forEach(addComponent);
}

function addLink(linkToAdd) {
    var navListItem = document.createElement("li");
    var anchorTag = document.createElement("a");
    anchorTag.href = linkToAdd.URL;
    anchorTag.innerHTML = linkToAdd.title;
    navListItem.appendChild(anchorTag);
    navlist.appendChild(navListItem);
}

function addStylesheet(filePath) {
    var linkTag = document.createElement("link");
    linkTag.rel = "stylesheet";
    linkTag.href = filePath;
    document.head.appendChild(linkTag);
}

function addComponent(componentToAdd) {
    if (componentToAdd.type.valueOf() === new String("heading").valueOf()) {
        addHeading(componentToAdd);
    }
    else if (componentToAdd.type.valueOf() === new String("paragraph").valueOf()) {
        addParagraph(componentToAdd);
    }
    else if (componentToAdd.type.valueOf() === new String("image").valueOf()) {
        addImage(componentToAdd);
    }
    else if (componentToAdd.type.valueOf() === new String("list").valueOf()) {
        addList(componentToAdd);
    }
    else if (componentToAdd.type.valueOf() === new String("video").valueOf()) {
        addVideo(componentToAdd);
    }
    else if (componentToAdd.type.valueOf() === new String("slideshow").valueOf()) {
        addSlideShow(componentToAdd);
    }
    else {
        alert("A page component could not be loaded because of an invalid type field.");
    }
}

function addHeading(headingToAdd) {
    var hTag = document.createElement("h2");
    hTag.innerHTML = headingToAdd.text;
    hTag.style.fontFamily = headingToAdd.font;
    hTag.style.fontSize = headingToAdd.fontSize;
    mainContent.appendChild(hTag);
}

function addParagraph(paragraphToAdd) {
    var pTag = document.createElement("p");
    pTag.innerHTML = paragraphToAdd.text;
    pTag.style.fontFamily = paragraphToAdd.font;
    pTag.style.fontSize = paragraphToAdd.fontSize;
    mainContent.appendChild(pTag);
}

function addImage(imageToAdd) {
    var imgTag = document.createElement("img");
    imgTag.className = "componentImage";
    imgTag.src = imageToAdd.src;
    imgTag.style.width = imageToAdd.width;
    imgTag.style.height = imageToAdd.height;
    
    if (imageToAdd.align.valueOf() === new String("right").valueOf()) {
        imgTag.style.marginLeft = "auto";
    }
    else if (imageToAdd.align.valueOf() === new String("center").valueOf()) {
        imgTag.style.marginLeft = "auto";
        imgTag.style.marginRight = "auto";
    }
    else {
        imgTag.style.marginRight = "auto";
    }
    
    mainContent.appendChild(imgTag);
}

function addList(listToAdd) {
    var ulTag = document.createElement("ul");
    ulTag.style.fontFamily = listToAdd.font;
    ulTag.style.fontSize = listToAdd.fontSize;
    
    for (i = 0; i < listToAdd.items.length; i++) {
        var liTag = document.createElement("li");
        liTag.innerHTML = listToAdd.items[i];
        ulTag.appendChild(liTag);
    }
    
    mainContent.appendChild(ulTag);
}

function addVideo(videoToAdd) {
    var videoTag = document.createElement("video");
    var sourceTag = document.createElement("source");
    videoTag.width = videoToAdd.width;
    videoTag.height = videoToAdd.height;
    videoTag.controls = true;
    sourceTag.src = videoToAdd.src;
    sourceTag.type = "video/webm";
    videoTag.appendChild(sourceTag);
    mainContent.appendChild(videoTag);
}

function addSlideShow(slideShowToAdd) {
    var slideShowID = slideShowList.length;
    var newSlideShow = {
        currentSlide: 0,
        playTimer: null,
        numSlides: slideShowToAdd.slides.length,
        slides: slideShowToAdd.slides
    };
    
    // This contains the slide image and caption
    var figureTag = document.createElement("figure");
    
    // This is the slide image
    var slideShowImageTag = document.createElement("img");
    slideShowImageTag.id = "slideImage-" + slideShowID;
    slideShowImageTag.className = "slideShowImage";
    slideShowImageTag.src = "";
    slideShowImageTag.alt = "Slide Show Image";
    slideShowImageTag.style.height = slideShowToAdd.height;
    
    // This is the caption
    var figcaptionTag = document.createElement("figcaption");
    figcaptionTag.id = "slideCaption-" + slideShowID;
    figcaptionTag.style.fontFamily = slideShowToAdd.font;
    figcaptionTag.style.fontSize = slideShowToAdd.fontSize;
    
    // This contains the slide show controls
    var menuTag = document.createElement("menu");
    
    // This button goes to the previous slide
    var prevButtonTag = document.createElement("button");
    prevButtonTag.id = "btnPrev-" + slideShowID;
    prevButtonTag.type = "button";
    prevButtonTag.onclick = function() {previousSlide(slideShowID);};
    
    // This button plays the slide show
    var playPauseButtonTag = document.createElement("button");
    playPauseButtonTag.id = "btnPlayPause-" + slideShowID;
    playPauseButtonTag.type = "button";
    playPauseButtonTag.onclick = function() {play(slideShowID);};
    
    // This button goes to the next slide
    var nextButtonTag = document.createElement("button");
    nextButtonTag.id = "btnNext-" + slideShowID;
    nextButtonTag.type = "button";
    nextButtonTag.onclick = function() {nextSlide(slideShowID);};
    
    // This image contains the icon for previous slide
    var prevImageTag = document.createElement("img");
    prevImageTag.src = "./images/previous.png";
    prevImageTag.alt = "Previous Slide";
    
    // This image contains the icon for play slide show, or pause while playing
    var playPauseImageTag = document.createElement("img");
    playPauseImageTag.id = "imgPlayPause-" + slideShowID;
    playPauseImageTag.src = "./images/play.png";
    playPauseImageTag.alt = "Play Slide Show";
    
    // This image contains the icon for next slide
    var nextImageTag = document.createElement("img");
    nextImageTag.src = "./images/next.png";
    nextImageTag.alt = "Next Slide";
    
    // Put it all together
    figureTag.appendChild(slideShowImageTag);
    figureTag.appendChild(figcaptionTag);
    prevButtonTag.appendChild(prevImageTag);
    playPauseButtonTag.appendChild(playPauseImageTag);
    nextButtonTag.appendChild(nextImageTag);
    menuTag.appendChild(prevButtonTag);
    menuTag.appendChild(playPauseButtonTag);
    menuTag.appendChild(nextButtonTag);
    mainContent.appendChild(figureTag);
    mainContent.appendChild(menuTag);
    
    // Add slide show to list and show current slide
    slideShowList.push(newSlideShow);
    showCurrentSlide(slideShowID);
}

function showCurrentSlide(slideShowID) {
    var slideShow = slideShowList[slideShowID];
    var slide = slideShow.slides[slideShow.currentSlide];
    
    document.getElementById("slideImage-" + slideShowID).src =
            slide.imagePath;
    document.getElementById("slideCaption-" + slideShowID).innerHTML =
            slide.caption;
}

function previousSlide(slideShowID) {
    var slideShow = slideShowList[slideShowID];
    if (slideShow.currentSlide === 0) {
        slideShow.currentSlide = slideShow.numSlides - 1;
    }
    else {
        --slideShow.currentSlide;
    }
    showCurrentSlide(slideShowID);
}

function nextSlide(slideShowID) {
    var slideShow = slideShowList[slideShowID];
    slideShow.currentSlide = (slideShow.currentSlide + 1) % slideShow.numSlides;
    showCurrentSlide(slideShowID);
}

function play(slideShowID) {
    var imgPlayPause = document.getElementById("imgPlayPause-" + slideShowID);
    var btnPlayPause = document.getElementById("btnPlayPause-" + slideShowID);
    imgPlayPause.src = "./images/pause.png";
    imgPlayPause.alt = "Pause Slide Show";
    btnPlayPause.onclick = function() {pause(slideShowID);};
    slideShowList[slideShowID].playTimer = 
            setInterval(function() {nextSlide(slideShowID);}, PLAY_INTERVAL);
}

function pause(slideShowID) {
    clearInterval(slideShowList[slideShowID].playTimer);
    var imgPlayPause = document.getElementById("imgPlayPause-" + slideShowID);
    var btnPlayPause = document.getElementById("btnPlayPause-" + slideShowID);
    imgPlayPause.src = "./images/play.png";
    imgPlayPause.alt = "Play Slide Show";
    btnPlayPause.onclick = function() {play(slideShowID);};
}